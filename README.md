# TempsEmoji 🌞
Aquest programa utilitza la llibreria Mastodon.py de Python i la API d'OpenWeatherMap per publicar en un compte de Mastodon amb el mapa del temps de Catalunya actual, utilitzant emojis per representar l'estat del temps.

![](https://nuvol.cat/temps_emoji.png)

## Requisits
- Una clau d'API d'OpenWeatherMap: Aquesta es pot obtenir gratuïtament des de [OpenWeatherMap](https://openweathermap.org/api)
- Una clau d'Autentificació de Mastodon: Es pot obtenir des de `https://[elteuservidor.mastodon]/settings/applications`
- Python 3 instal·lat en l'ordinador
- Les llibreries `Mastodon.py`,`requests` i `json`

## Utilització
1. Inserir les claus d'API d'OpenWeatherMap i Mastodon a les variables corresponents al codi.
2. Executar el programa utilitzant python3.
3. El programa es repeteix cada 4 hores i publica un nou tweet amb les dades actualitzades.

## Nota
- Els codis postals de Catalunya utilitzats per obtenir les dades del temps estan en la variable `c`. Si es volen afegir o eliminar codis postals, aquesta variable s'ha de modificar.
- El programa pot ser modificat per publicar tweets amb una freqüencia diferent.

Aquest codi està disponible sota la llicència MIT. Utilitzeu-lo a voluntat i si us plau, no oblideu citar la font
